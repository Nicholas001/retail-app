package com.nicholas.retail.utils;

import java.util.List;

public abstract class GenericStore<T> {

    public abstract T save(T entity);

    public abstract List<T> findAll();

    public abstract T findById (int id);

    public abstract void deleteById (int id);

    public abstract void deleteAll();

    public abstract T update (T entity);


}
