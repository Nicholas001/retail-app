package com.nicholas.retail.product;

import com.nicholas.retail.utils.GenericStore;

import java.util.List;

public class ProductStore extends GenericStore<Product> {

    private List<Product> products;
    private ProductFileReader productFileReader;

    public ProductStore() {
        this.productFileReader = new ProductFileReader();
//      this.products = new LinkedList<>();
        this.products = productFileReader.readProductsFromFile();
    }

    public Product save(Product product) {
        product.setId(generateId());
        products.add(product);

        return product;
    }


    public List<Product> findAll() {
        return products;
    }

    public Product findById(int productId) {
        for (Product product : products) {
            if (product.getId() == productId) {
                return product;
            }
        }
        return null;
    }

    public void deleteById(int productId) {
        for (Product product : products) {
            if (product.getId() == productId) {
                products.remove(product);
                break;
            }
        }
    }

    public void deleteAll() {
        for (Product product : products) {
//            deleteProductById(product.getId());
            products.remove(product);
        }
    }

    public Product update(Product product) {
        for (Product curentProduct : products) {
            if (curentProduct.getId() == product.getId()) {
                products.remove(curentProduct);
                products.add(product);
                break;
            }
        }
        return product;
    }


    private int generateId() {
        int maxId = 0;
        for (Product product : products) {
            if (product.getId() > maxId) {
                maxId = product.getId();
            }
        }
        return maxId + 1;
    }
}
