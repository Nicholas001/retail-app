package com.nicholas.retail.product;

import com.nicholas.retail.utils.Paths;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class ProductFileReader {

    public List<Product> readProductsFromFile() {

        List<Product> prodocts = new LinkedList<>();


        try {
            BufferedReader buffer = new BufferedReader(
                    new FileReader(Paths.PATH_TO_PRODUCT_FILE));
            String line = null;
            line = buffer.readLine();


            while (line != null) {
                String[] elements = line.split(" ");
                Product product = new Product();
                product.setId(Integer.parseInt(elements[0]));
                product.setDenumire(elements[1]);
                product.setPrice(Double.parseDouble(elements[2]));
                product.setBarcode(elements[3]);
                product.setQuantity(Integer.parseInt(elements[4]));
                prodocts.add(product);
                line = buffer.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prodocts;
    }
}


