package com.nicholas.retail.product;

import java.util.Scanner;

public class ProductConsoleReader {

    public Product readProduct(Scanner scanner) {

        Product product = new Product();

        System.out.println(" Please insert a product! ");
        System.out.println(" Please insert description ");
        product.setDenumire(scanner.next());
        System.out.println("Please insert price ");
        product.setPrice(Double.parseDouble(scanner.next()));
        System.out.println("Please insert barcode");
        product.setBarcode(scanner.next());
        System.out.println("Please insert quantity");
        product.setQuantity(Integer.parseInt(scanner.next()));
        return product;
    }

}
