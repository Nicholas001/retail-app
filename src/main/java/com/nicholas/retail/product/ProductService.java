package com.nicholas.retail.product;

import java.util.List;

public class ProductService {

    private ProductStore productStore;

    public ProductService() {
        this.productStore = new ProductStore();
    }

    public Product saveProduct (Product product){
        return productStore.save(product);
    }

    public List<Product> fiindAllProducts (){
        return productStore.findAll();
    }

    public Product findProductById (int id){
        return productStore.findById(id);
    }

    public void deleteProductById (int id){
        productStore.deleteById(id);
    }

    public void deleteAllProducs (){
        productStore.deleteAll();
    }

    public Product updateProduct (Product product){
        return productStore.update(product);
    }

}
