package com.nicholas.retail.product;

import java.util.List;
import java.util.Scanner;

public class ProductConsoleWritter {

    private ProductService productService;
    private ProductConsoleReader productConsoleReader;

    public ProductConsoleWritter() {
        this.productService = new ProductService();
        this.productConsoleReader = new ProductConsoleReader();
    }

    public void productOptions(Scanner scanner) {

        boolean isWorking = true;

        while (isWorking) {
            System.out.println(" Options for products are: ");
            System.out.println("\t1. Display all products ");
            System.out.println("\t2. Insert new product ");
            System.out.println("\t3. Get product by ID");
            System.out.println("\t4. Update product");
            System.out.println("\t5. Delete product ");
            System.out.println("\t6. Exit ");
            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    displayAllProducts();
                    break;
                case 2:
                    insertNewProduct(scanner);
                    break;
                case 3:
                    displayProductById(scanner);
                    break;
                case 4:
                    updateProduct(scanner);
                    break;
                case 5:
                    deleteProductById(scanner);
                    break;
                case 6:
                    isWorking = false;
                    break;
                default:
                    System.out.println("Please insert a valid option! ");
            }
        }
    }

    private void displayAllProducts() {
        List<Product> products = productService.fiindAllProducts();
        for (Product product : products) {
            System.out.println(product);
        }
    }

    private void insertNewProduct(Scanner scanner) {
        Product product = productConsoleReader.readProduct(scanner);
        productService.saveProduct(product);
    }

    private void displayProductById(Scanner scanner) {
        System.out.println("Please insert product ID");
        int id = scanner.nextInt();
        Product product = productService.findProductById(id);
        System.out.println(product);
    }

    private void updateProduct(Scanner scanner) {
        Product product = productConsoleReader.readProduct(scanner);
        System.out.println("Please insert product ID ");
        int id = scanner.nextInt();
        product.setId(id);
        productService.updateProduct(product);
    }

    private void deleteProductById(Scanner scanner) {
        System.out.println("Please insert ID ");
        int id = scanner.nextInt();
        productService.deleteProductById(id);
    }

    public void displayAllProductQuantity() {
        List<Product> products = productService.fiindAllProducts();
        for (Product product : products) {
            System.out.println(product.getId() + " " + product.getDenumire() + " " + product.getQuantity());
        }
    }


}
