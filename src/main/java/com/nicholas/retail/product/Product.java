package com.nicholas.retail.product;

public class Product {

    private int id;
    private String denumire;
    private double price;
    private String barcode;
    private int quantity;

    public Product() {
    }

    public Product(int id, String denumire, double price, String barcode) {
        this.id = id;
        this.denumire = denumire;
        this.price = price;
        this.barcode = barcode;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", denumire='" + denumire + '\'' +
                ", price=" + price +
                ", barcode='" + barcode + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
