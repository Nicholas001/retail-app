package com.nicholas.retail.runner;

import com.nicholas.retail.client.ClientConsoleWritter;
import com.nicholas.retail.product.ProductConsoleWritter;

import java.util.Scanner;

public class AppConsole {

    ClientConsoleWritter clientConsoleWritter;
    ProductConsoleWritter productConsoleWritter;

    public AppConsole() {
        clientConsoleWritter = new ClientConsoleWritter();
        productConsoleWritter = new ProductConsoleWritter();
    }

    public void runApplication() {

        System.out.println("Welcom to our retail application!");

        boolean test = true;

        while (test) {

            System.out.println("Choose option: ");
            System.out.println("\t1. Clients");
            System.out.println("\t2. Products");
            System.out.println("\t3. Stock");
            System.out.println("\t4. Orders");
            System.out.println("\t5. Exit ");

            Scanner sc = new Scanner(System.in);
            int option = sc.nextInt();

            switch (option) {
                case 1:
                    clientConsoleWritter.clientOptions(sc);
                    break;
                case 2:
                    productConsoleWritter.productOptions(sc);
                    break;
                case 3:
                    productConsoleWritter.displayAllProductQuantity();
                    break;
                case 5:
                    test = false;
                    break;

                default:
                    System.out.println("The selected option doesn`t exist! ");
            }

        }
    }
}
