package com.nicholas.retail.client;

import com.nicholas.retail.utils.GenericStore;

import java.util.List;

public class ClientStore extends GenericStore<Client> {

    private List<Client> allClients;
    private ClientFileReader clientFileReader;

    public ClientStore() {
        clientFileReader = new ClientFileReader();
        allClients = clientFileReader.readClientFromFile();
    }

//    public void initializare() {
//        allClients = clientFileReader.readClientFromFile();
//    }

    public Client save(Client client) {
        client.setId(generateId());
        allClients.add(client);
        return client;
    }


    public List<Client> findAll() {
        return allClients;
    }

    public Client findById(int id) {
        for (Client client : allClients) {
            if (client.getId() == id) {
                return client;
            }
        }
        return null;
    }

    public void deleteById(int id) {
        for (Client client : allClients) {
            if (client.getId() == id) {
                allClients.remove(client);
                break;
            }
        }
    }

    public void deleteAll() {
        for (Client client : allClients){
            allClients.remove(client);
        }
    }

    public Client update(Client client) {
        for (Client curentClient : allClients) {
            if (curentClient.getId() == client.getId()) {
                allClients.remove(curentClient);
                allClients.add(client);
                return client;
            }
        }
        return null;
    }


    private int generateId() {
        int maxId = 0;
        for (Client client : allClients) {
            if (maxId < client.getId()) ;
            maxId = client.getId();
        }
        return maxId + 1;
    }

}
