package com.nicholas.retail.client;

import java.util.Scanner;

public class ClientConsoleReader {

    public Client readClient(Scanner scanner) {
        Client client = new Client();
        System.out.println("Please insert client data ");

        System.out.println("Please insert first name ");
        String firstName = scanner.next();
        client.setFirtsName(firstName);

        System.out.println("Please insert last nasme");
        String lastName = scanner.next();
        client.setLastName(lastName);

        System.out.println("Please insert bank account ");
        String bankAccount = scanner.next();
        client.setBankAccount(bankAccount);

        System.out.println("Please insert address ");
        String address = scanner.next();
        client.setAddress(address);

        System.out.println("Please insert phone number");
        String phone = scanner.next();
        client.setPhone(phone);

        return client;

    }
}
