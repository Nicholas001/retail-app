package com.nicholas.retail.client;

public class Client {

    private int id;
    private String firtsName;
    private String lastName;
    private String address;
    private String bankAccount;
    private String phone;

    public Client(int id, String firtsName, String lastName, String address, String bankAccount, String phone) {
        this.id = id;
        this.firtsName = firtsName;
        this.lastName = lastName;
        this.address = address;
        this.bankAccount = bankAccount;
        this.phone = phone;
    }

    public Client() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirtsName() {
        return firtsName;
    }

    public void setFirtsName(String firtsName) {
        this.firtsName = firtsName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Client{" + "id=" + id + ", firtsName='" + firtsName + '\'' + ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' + ", bankAccount='" + bankAccount + '\'' + ", phone='" + phone + '\'' + '}';
    }




}
