package com.nicholas.retail.client;

import java.util.List;

public class ClientService {

    private ClientStore clientStore;

    public ClientService() {
        this.clientStore = new ClientStore();
    }

    public Client saveClient(Client client) {
        return clientStore.save(client);
    }

    public List<Client> getAllClient() {
        return clientStore.findAll();
    }

    public Client getClientById(int id) {
        return clientStore.findById(id);
    }

    public void deleteClientById(int id) {
        clientStore.deleteById(id);
    }

    public Client updateClient(Client client) {
        return clientStore.update(client);
    }

//    public void initializare (){
//        this.clientStore.initializare();
//    }

}
