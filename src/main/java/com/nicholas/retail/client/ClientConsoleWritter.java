package com.nicholas.retail.client;

import java.util.List;
import java.util.Scanner;

public class ClientConsoleWritter {

    private ClientService clientService;
    private ClientConsoleReader clientConsoleReader;

    public ClientConsoleWritter() {
        this.clientService = new ClientService();
        this.clientConsoleReader = new ClientConsoleReader();
    }

    public void clientOptions(Scanner scanner) {
//        this.clientService.initializare();
        boolean isWorking = true;

        while (isWorking) {

            System.out.println("Options for client are: ");
            System.out.println("\t1. Display all clients ");
            System.out.println("\t2. Display client by ID ");
            System.out.println("\t3. Delete client ");
            System.out.println("\t4. Update client ");
            System.out.println("\t5. Insert new client ");
            System.out.println("\t6. Exit");

            int options = scanner.nextInt();


            switch (options) {
                case 1:
                    displayAllClients(clientService.getAllClient());
                    break;
                case 2:
                    displayClientById(scanner);
                    break;
                case 3:
                    deleteClientById(scanner);
                    break;
                case 4:
                    updateClient(scanner);
                    break;
                case 5:
                    insertNewClient(scanner);
                    break;
                case 6:
                    isWorking = false;
                    break;
                default:
                    System.out.println("Please choose another option!");
            }
        }
    }

    private void displayAllClients(List<Client> clients) {
        for (Client client : clients) {
            displayClient(client);
        }
    }

    private void displayClient(Client client) {
        System.out.println(client);
    }

    private void displayClientById(Scanner scanner) {
        System.out.println("Please insert client ID");
        int id = scanner.nextInt();
        Client client = clientService.getClientById(id);
        displayClient(client);
    }

    private void deleteClientById(Scanner scanner) {
        int id = scanner.nextInt();
        clientService.deleteClientById(id);
    }

    private void insertNewClient(Scanner scanner) {
        Client client = clientConsoleReader.readClient(scanner);
        if (clientService.saveClient(client) != null) {
            System.out.println("Success ");
        } else {
            System.out.println("Failure!");
        }
    }

    private void updateClient(Scanner scanner) {
        System.out.println("Please insert client id ");
        int id = scanner.nextInt();
        Client client = clientConsoleReader.readClient(scanner);
        client.setId(id);
        if (clientService.updateClient(client) != null) {
            System.out.println("Success ");
        } else {
            System.out.println("Failure!");
        }
    }


}
