package com.nicholas.retail.client;

import com.nicholas.retail.utils.Paths;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class ClientFileReader {

    public List<Client> readClientFromFile() {
        List<Client> clients = new LinkedList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(Paths.PATH_TO_CLIENT_FILE));
            String line = reader.readLine();
            while (line != null) {
                String[] elements = line.split(" ");
                Client client = new Client();
                client.setId(Integer.parseInt(elements[0]));
                client.setFirtsName(elements[1]);
                client.setLastName(elements[2]);
                client.setAddress(elements[3]);
                client.setBankAccount(elements[4]);
                client.setPhone(elements[5]);
                clients.add(client);
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return clients;
    }


}
