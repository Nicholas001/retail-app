package com.nicholas.retail.client;

import org.junit.Assert;
import org.junit.Test;

public class TestClientStoreMethods {

    @Test
    public void testSaveClient() {

        ClientStore cs = new ClientStore();
//        cs.initializare();
        Client testClient = new Client();
        cs.save(testClient);
        Assert.assertEquals(7, testClient.getId());
    }


}
