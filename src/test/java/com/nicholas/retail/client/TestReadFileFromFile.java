package com.nicholas.retail.client;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class TestReadFileFromFile {

    @Test
    public void testreadFileFromFile() {

        ClientFileReader cfr = new ClientFileReader();
        List<Client> test = cfr.readClientFromFile();
        Assert.assertEquals(6, test.size());

    }

}
